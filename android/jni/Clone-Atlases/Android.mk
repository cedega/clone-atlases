LOCAL_PATH := ../../clone-atlases/

include $(CLEAR_VARS)

LOCAL_MODULE := Clone-Atlases
LOCAL_C_INCLUDES := $(LOCAL_PATH)/include ../../clone/include
LOCAL_CFLAGS += -O3

LOCAL_SRC_FILES := $(subst $(LOCAL_PATH)/,,$(wildcard $(LOCAL_PATH)/src/*.cpp))
LOCAL_SRC_FILES := $(LOCAL_SRC_FILES) $(subst $(LOCAL_PATH)/,,$(wildcard $(LOCAL_PATH)/src/*/*.cpp))
LOCAL_SRC_FILES := $(LOCAL_SRC_FILES) $(subst $(LOCAL_PATH)/,,$(wildcard $(LOCAL_PATH)/src/*/*/.cpp))

LOCAL_SRC_FILES := $(LOCAL_SRC_FILES) $(subst $(LOCAL_PATH)/,,$(wildcard $(LOCAL_PATH)/src/*.c))
LOCAL_SRC_FILES := $(LOCAL_SRC_FILES) $(subst $(LOCAL_PATH)/,,$(wildcard $(LOCAL_PATH)/src/*/*.c))
LOCAL_SRC_FILES := $(LOCAL_SRC_FILES) $(subst $(LOCAL_PATH)/,,$(wildcard $(LOCAL_PATH)/src/*/*/*.c))

LOCAL_SHARED_LIBRARIES := Clone SDL2 libstlport
LOCAL_LDLIBS :=

include $(BUILD_SHARED_LIBRARY)
