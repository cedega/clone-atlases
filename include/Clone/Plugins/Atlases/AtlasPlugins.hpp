#pragma once
#include <string>
#include "Clone/Core/Plugin.hpp"

namespace cl
{
    
class TextureAtlas;

class AtlasPlugins : public cl::Plugin
{
 public:
    AtlasPlugins();
    
    void processMakeAtlas(const std::string &xml_file, cl::TextureAtlas *output);
    
 private:

};

}