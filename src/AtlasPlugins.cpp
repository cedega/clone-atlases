#include "Clone/External/tinyxml2.h"
#include "Clone/Data/TextureAtlas.hpp"
#include "Clone/Plugins/Atlases/AtlasPlugins.hpp"

namespace cl
{
    
AtlasPlugins::AtlasPlugins()
{
    setPluginType(cl::Plugin::AssetConditioner);
}

void AtlasPlugins::processMakeAtlas(const std::string &xml_file, cl::TextureAtlas *output)
{
    tinyxml2::XMLDocument doc;
    doc.Parse(xml_file.c_str(), xml_file.size());

    tinyxml2::XMLElement *root = doc.FirstChildElement()->FirstChildElement()->FirstChildElement();
    std::string item_name;

    while (root != NULL)
    {
        tinyxml2::XMLElement *item = root->FirstChildElement()->FirstChildElement();

        while (item != NULL)
        {
            glm::vec4 bounds;
            bounds.x = static_cast<float>(item->FirstAttribute()->Next()->IntValue());
            bounds.y = static_cast<float>(item->FirstAttribute()->Next()->Next()->IntValue());
            bounds.z = static_cast<float>(item->FirstAttribute()->Next()->Next()->Next()->IntValue());
            bounds.w = static_cast<float>(item->FirstAttribute()->Next()->Next()->Next()->Next()->IntValue());

            item_name = item->FirstAttribute()->Value();
            item_name += ".png";
            output->add(item_name, bounds);

            item = item->NextSiblingElement();
        }

        root = root->NextSiblingElement();
    }
}

}